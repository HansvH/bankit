/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package bankIT;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.math.BigInteger;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

class AppTest {

    public static final String BANK_URI = "http://localhost:9090/bank/";
    Client client = ClientBuilder.newClient();
    WebTarget target;

    @BeforeEach
    public void init() {
        target = client.target(BANK_URI);
    }

    @Test
    void when_AccessAccount_2accountsReturned() {
        int expected = 2;
        WebTarget accountPath = target.path("accounts");

        List<Account> response = accountPath.request().get(List.class);

        assertEquals(expected, response.size());

    }

    @Test
    void when_AccessAccount1_accountReturned(){
        Account accountExpected = new Account(Long.valueOf(1), "A1", BigInteger.valueOf(100));
        WebTarget accountPath = target.path("accounts/1");

        Account response = accountPath.request().get(Account.class);

        assertNotNull(response);
        assertEquals(accountExpected, response);

    }


    @Test
    void when_PostAccount_Status201Returned(){
        String expected = "http://localhost:9090/bank/accounts/3";
        int expectedStatus = 201;
        Account newAccount = new Account(Long.valueOf(199), "NewAccount77", BigInteger.valueOf(77));
        WebTarget accountPath = target.path("accounts");

        Builder invocationBuilder = accountPath.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(newAccount, MediaType.APPLICATION_JSON));


        assertEquals(expectedStatus, response.getStatus() );
        assertEquals(expected, response.getLocation().toString());
    }

}
