package bankIT;

import java.math.BigInteger;
import java.util.Objects;

import javax.xml.bind.annotation.XmlRootElement;

// @XmlRootElement
public class Account {

    private  Long id;
    private  String accountNr;
    private  BigInteger balance;

    public Account() {
    }

    public Account(Long id, String accountNr, BigInteger balance) {
        this.id = id;
        this.accountNr = accountNr;
        this.balance = balance;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountNr() {
        return this.accountNr;
    }

    public void setAccountNr(String accountNr) {
        this.accountNr = accountNr;
    }

    public BigInteger getBalance() {
        return this.balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public Account id(Long id) {
        this.id = id;
        return this;
    }

    public Account accountNr(String accountNr) {
        this.accountNr = accountNr;
        return this;
    }

    public Account balance(BigInteger balance) {
        this.balance = balance;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Account)) {
            return false;
        }
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(accountNr, account.accountNr) && Objects.equals(balance, account.balance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNr, balance);
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", accountNr='" + getAccountNr() + "'" +
            ", balance='" + getBalance() + "'" +
            "}";
    }


}
